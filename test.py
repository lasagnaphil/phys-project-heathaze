import numpy as np
from odeintw import odeintw
# from sympy import *
import matplotlib.pyplot as plt


def yfunc_1(y, x):
    y1, y2 = y
    dx = 0.001
    dy = 0.001
    return [y2, N_y(x, y1, dy) - N_x(x, y1, dx) * y2]

def yjac_1(y, x):
    y1, y2 = y
    dx = 0.001
    dy = 0.001
    jac = np.array
    (
        [0, 1],
        [dy_N_y(x, y1, dy) - dy_N_x(x, y1, dx, dy) * y2, -N_x(x, y1, dx)]
    )
    return jac

def yfunc_2(y, x):
    y1, y2 = y
    dx = 0.001
    dy = 0.001
    return [y2, (N_y(x, y1, dy) - N_x(x, y1, dx)*y2)*(1+y2**2)]

def yjac_2(y, x):
    y1, y2 = y
    dx = 0.001
    dy = 0.001
    jac = np.array
    (
        [0, 1],
        [(dy_N_y(x, y1, dy) - dy_N_x(x, y1, dx, dy) * y2)*(1+y2**2), -N_x(x, y1, dx)*(1+3*y2**2)]
    )
    return jac


def n(x, y):
    return 1 - 1*y*y


def N_x(x, y, dx):
    return (n(x+dx, y)-n(x, y))/dx


def dy_N_x(x, y, dx, dy):
    return (N_x(x, y+dy, dx) - N_x(x, y, dx))/dy


def N_y(x, y, dy):
    return (n(x, y+dy)-n(x, y))/dy


def dy_N_y(x, y, dy):
    return (N_y(x, y+dy, dy) - N_y(x, y, dy))/dy


# Initial conditions.
y0 = np.array([0, 0.01])

# Desired time samples for the solutions
x = np.linspace(0, 5, 101)

# Parameters
k = 0.2

y, infodict = odeintw(yfunc_2, y0, x, Dfun=yjac_2, full_output=True)

color1 = (0.2, 0.2, 1.0)
color2 = (1.0, 0.2, 0.2)
plt.plot(x, y[:, 0], color=color1, label='y1', linewidth=1.5)
plt.plot(x, y[:, 1], color=color2, label='y2', linewidth=1.5)
plt.xlabel('x')
plt.grid(True)
plt.legend(loc='best')
plt.show()




