import numpy as np
import matplotlib.pyplot as plt
import gaussfield

N = 64


def example1():
    X, Y = np.mgrid[0:1:1./N, 0:1:1./N]
    field = gaussfield.GaussianField2d(1.0, 1.0, 3, 12345)
    for i in range(0, 1):
        field.advance(0.001)
        print 'sampling field...'
        F = field.sample(X, Y)
        print 'finished'
        plt.imshow(F[:,:,0]**2 + F[:,:,1]**2)
        plt.show()
        print F


def example2():
    X, Y, Z = np.mgrid[0:1:1./N, 0:1:1./N,0:1:1./N]
    field = gaussfield.GaussianField3d(1.0, 1.0, 3, 12345)
    field.advance(1.0)
    print 'sampling field...'
    F = field.sample(X, Y, Z)
    print 'finished'
    F2 = F[...,0]**2 + F[...,1]**2 + F[...,2]**2
    print 'mean power:', F2.mean()
    plt.imshow(F2[:,:,0])
    plt.show()
    print F


if __name__ == '__main__':
    example1()
    example2()